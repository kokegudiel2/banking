package main

import (
	"gitlab.com/kokegudiel2/banking/app"
	"gitlab.com/kokegudiel2/banking/logger"
)

func main() {
	logger.Info("Starting application...")
	app.Start()
}
