package app

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/errs"
	"gitlab.com/kokegudiel2/banking/mocks/service"
)

var (
	_router      *mux.Router
	_c           CustomerHandlers
	_mockService *service.MockCustomerService
)

func setup(t *testing.T) func() {
	ctrl := gomock.NewController(t)
	_mockService = service.NewMockCustomerService(ctrl)
	_c = CustomerHandlers{_mockService}

	_router = mux.NewRouter()
	_router.HandleFunc("/customers", _c.getAllCustomer)
	return func() {
		_router = nil
		ctrl.Finish()
	}
}

//Status code 200 to approve.
//Error not receiving the code.
func TestCustomerHandler(t *testing.T) {
	teardown := setup(t)
	defer teardown()

	dummyCustomers := []dto.CustomerResponse{
		{"1001", "Ashish", "New Delhi", "110011", "2000-01-01", "1"},
		{"1002", "Rob", "New Delhi", "110011", "2000-01-01", "1"},
	}
	_mockService.EXPECT().GetAllCustomers("").Return(dummyCustomers, nil)
	c := CustomerHandlers{_mockService}

	router := mux.NewRouter()
	router.HandleFunc("/customers", c.getAllCustomer)

	request, _ := http.NewRequest(http.MethodGet, "/customers", nil)

	//Act
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, request)

	//Assert
	if recorder.Code != http.StatusOK {
		t.Error("Error while testing the status code [CustomerHandler]")
	}
}

//TestCustomerHandlerErrorMessage should
//return status code 500 with error msg
func TestCustomerHandlerErrorMessage(t *testing.T) {
	teardown := setup(t)
	defer teardown()

	_mockService.EXPECT().GetAllCustomers("").Return(nil, errs.NewUnexpectedError("Database error [CustomerHandler]"))

	request, _ := http.NewRequest(http.MethodGet, "/customers", nil)

	//Act
	recorder := httptest.NewRecorder()
	_router.ServeHTTP(recorder, request)

	//Assert
	if recorder.Code != http.StatusInternalServerError {
		t.Error("Error while testing the status code [CustomerHandler]")
	}
}
