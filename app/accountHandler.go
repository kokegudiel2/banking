package app

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/service"
)

//AccountHandler ...
type AccountHandler struct {
	srvc service.AccountService
}

//NewAccount create a new account
func (h AccountHandler) NewAccount(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	customerID := vars["customer_id"]

	var request dto.NewAccountRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	request.CustumerID = customerID
	account, appError := h.srvc.NewAccount(request)
	if appError != nil {
		http.Error(w, appError.Message, appError.Code)
		return
	}

	writeResponse(w, http.StatusCreated, account)
}

func (h AccountHandler) MakeTransaction(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	accountID := vars["account_id"]
	customerID := vars["customer_id"]

	var request dto.TransactionRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	request.AccountID = accountID
	request.CustomerID = customerID

	account, err := h.srvc.MakeTransaction(request)
	if err != nil {
		http.Error(w, err.Message, err.Code)
		return
	}

	writeResponse(w, http.StatusOK, account)
}
