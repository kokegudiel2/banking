package app

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/kokegudiel2/banking/service"
)

//CustomerHandlers ...
type CustomerHandlers struct {
	srvc service.CustomerService
}

func (c *CustomerHandlers) getAllCustomer(w http.ResponseWriter, r *http.Request) {
	status := r.URL.Query().Get("status")

	customers, err := c.srvc.GetAllCustomers(status)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
		return
	}

	writeResponse(w, http.StatusOK, customers)
}

func (c *CustomerHandlers) getCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["customer_id"]

	customer, err := c.srvc.GetCustomer(id)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
		return
	}

	writeResponse(w, http.StatusOK, customer)
}

func writeResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(err)
	}
}
