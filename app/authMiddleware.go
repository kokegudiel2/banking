package app

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/kokegudiel2/banking/domain"
	"gitlab.com/kokegudiel2/banking/errs"
)

type AuthMiddleware struct {
	repo domain.AuthRepository
}

func (a AuthMiddleware) authorizationHandler() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			currentRoute := mux.CurrentRoute(r)
			currenRouteVars := mux.Vars(r)
			authHeader := r.Header.Get("Authorization")

			if authHeader != "" {
				token := getTokenFromHeader(authHeader)

				isAuthorized := a.repo.IsAuthorized(token, currentRoute.GetName(), currenRouteVars)

				if isAuthorized {
					next.ServeHTTP(rw, r)
				} else {
					err := errs.AppError{
						Code:    http.StatusForbidden,
						Message: "Unauthorized",
					}
					writeResponse(rw, err.Code, err.Message)
				}
			} else {
				writeResponse(rw, http.StatusUnauthorized, "token missing")
			}
		})
	}
}

func getTokenFromHeader(header string) string {
	token := strings.Split(header, "Bearer")
	if len(token) == 2 {
		return strings.TrimSpace(token[1])
	}

	return ""
}
