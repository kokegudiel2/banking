package app

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kokegudiel2/banking/domain"
	"gitlab.com/kokegudiel2/banking/logger"
	"gitlab.com/kokegudiel2/banking/service"
)

var (
	db = getDBClient()
	ch = CustomerHandlers{service.NewCustomerService(domain.NewCustomerRepositoryDB(db))}
	ah = AccountHandler{service.NewAccountService(domain.NewAccountRepositoryDB(db))}
	m  = AuthMiddleware{repo: domain.NewAuthRepository()}
)

//Start up server
func Start() {
	sanityCheck()
	router := mux.NewRouter()

	//Endṕoints
	router.HandleFunc("/customers", ch.getAllCustomer).
		Methods("GET").
		Name("GetAllCustomers")
	router.HandleFunc("/customers/{customer_id:[0-9]+}", ch.getCustomer).
		Methods("GET").
		Name("GetCustomer")

	router.HandleFunc("/customers/{customer_id:[0-9]+}/account", ah.NewAccount).
		Methods("POST").
		Name("NewAccount")
	router.HandleFunc("/customers/{customer_id:[0-9]+}/account/{account_id:[0-9]+}", ah.MakeTransaction).
		Methods("POST").
		Name("NewTransaction")
	//Endṕoints

	router.Use(m.authorizationHandler())

	log.Println("Up server on ", address, port)
	log.Fatalln(http.ListenAndServe(":"+port, router))
}

var (
	address  = os.Getenv("SERVER_ADDRESS")
	port     = os.Getenv("SERVER_PORT")
	dbUser   = os.Getenv("DB_USER")
	dbPasswd = os.Getenv("DB_PASSWD")
	dbAddr   = os.Getenv("DB_ADDR")
	dbName   = os.Getenv("DB_NAME")
)

func sanityCheck() {
	envProps := []string{
		"SERVER_ADDRESS",
		"SERVER_PORT",
		"DB_USER",
		"DB_PASSWD",
		"DB_ADDR",
		"DB_NAME",
	}
	for _, k := range envProps {
		if os.Getenv(k) == "" {
			logger.Fatal(fmt.Sprintf("Environment variable %s not defined. Terminating application...", k))
		}
	}
}

func getDBClient() *sqlx.DB {
	dataSoruce := fmt.Sprintf("%s:%s@tcp(%s)/%s", dbUser, dbPasswd, dbAddr, dbName)
	client, err := sqlx.Open("mysql", dataSoruce)
	if err != nil {
		panic(err)
	}

	//Settings
	client.SetConnMaxLifetime(time.Minute * 3)
	client.SetMaxOpenConns(10)
	client.SetMaxIdleConns(10)

	return client
}
