package domain

//CustomerRepositoryStub struct for manipulate customer
type CustomerRepositoryStub struct {
	customer []Customer
}

//FindAll return customers from db
func (s CustomerRepositoryStub) FindAll() ([]Customer, error) {
	return s.customer, nil
}

//NewCustomerRepositoryStub instance CustomerRepositoryStub
func NewCustomerRepositoryStub() CustomerRepositoryStub {
	customer := []Customer{
		{"tumadre", "tumadre", "tumadre", "tumadre", "tumadre", "tumadre"},
		{"latuya", "latuya", "latuya", "latuya", "latuya", "latuya"},
	}

	return CustomerRepositoryStub{customer}
}
