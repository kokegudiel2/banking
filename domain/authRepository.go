package domain

import (
	"encoding/json"
	"net/http"
	"net/url"

	"gitlab.com/kokegudiel2/banking/logger"
)

type AuthRepository interface {
	IsAuthorized(token, routeName string, vars map[string]string) bool
}

type RemoteAuthRepository struct {
}

func (r RemoteAuthRepository) IsAuthorized(token, routeName string, vars map[string]string) bool {
	u := buildVerifyURL(token, routeName, vars)

	response, err := http.Get(u)
	if err != nil {
		logger.Error("Error while sending..." + err.Error())
		return false
	}

	m := make(map[string]bool)
	if err = json.NewDecoder(response.Body).Decode(&m); err != nil {
		logger.Error("Error while decoding response from auth server:" + err.Error())
		return false
	}

	return m["isAuthorized"]
}

func buildVerifyURL(token, routeName string, vars map[string]string) string {
	u := url.URL{
		Host:   "localhost:8888",
		Path:   "/auth/verify",
		Scheme: "http",
	}

	q := u.Query()
	q.Add("token", token)
	q.Add("routeName", routeName)
	for k, v := range vars {
		q.Add(k, v)
	}

	u.RawQuery = q.Encode()
	return u.String()
}

func NewAuthRepository() RemoteAuthRepository {
	return RemoteAuthRepository{}
}
