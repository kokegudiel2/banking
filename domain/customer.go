package domain

import (
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/errs"
)

//Customer struct for control
type Customer struct {
	ID          string `db:"customer_id"`
	Name        string
	City        string
	Zipcode     string
	DateofBirth string `db:"date_of_birth"`
	Status      string
}

//CustomerRepository interface Behavior
type CustomerRepository interface {
	FindAll(status string) ([]Customer, *errs.AppError)
	FindByID(string) (*Customer, *errs.AppError)
}

func (c Customer) statusAsText() string {
	statusAsText := "active"

	if c.Status == "0" {
		statusAsText = "inactive"
	}

	return statusAsText
}

//ToDTO convert Model to DTO
func (c Customer) ToDTO() dto.CustomerResponse {
	return dto.CustomerResponse{
		ID:          c.ID,
		Name:        c.Name,
		DateofBirth: c.DateofBirth,
		City:        c.City,
		Status:      c.statusAsText(),
		Zipcode:     c.Zipcode,
	}
}
