package domain

import (
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/errs"
)

//Account struct for control
//Rel with Customer
type Account struct {
	AccountID   string  `db:"account_id"`
	CustomerID  string  `db:"customer_id"`
	OpeningDate string  `db:"opening_date"`
	AccountType string  `db:"account_type"`
	Amount      float64 `db:"amount"`
	Status      string  `db:"status"`
}

//ToNewAccountResponseDTO convert account to dto
func (a Account) ToNewAccountResponseDTO() dto.NewAccountResponse {
	return dto.NewAccountResponse{
		AccountID: a.AccountID,
	}
}

func (a Account) CanWithdraw(amount float64) bool {
	return a.Amount < amount
}

//AccountRepository interface behaivor
//go:generate mockgen -destination=../mocks/domain/mockAccountRepository.go -package=domain gitlab.com/kokegudiel2/banking/domain AccountRepository
type AccountRepository interface {
	Save(Account) (*Account, *errs.AppError)
	FindBy(string) (*Account, *errs.AppError)
	SaveTransaction(Transaction) (*Transaction, *errs.AppError)
}
