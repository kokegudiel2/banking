package domain

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql" //Mariadb driver
	"github.com/jmoiron/sqlx"
	"gitlab.com/kokegudiel2/banking/errs"
	"gitlab.com/kokegudiel2/banking/logger"
)

//CustomerRepositoryDB struct for db operations
type CustomerRepositoryDB struct {
	client *sqlx.DB
}

//NewCustomerRepositoryDB instantiate the repository to operate the db
func NewCustomerRepositoryDB(dbClient *sqlx.DB) CustomerRepositoryDB {
	return CustomerRepositoryDB{dbClient}
}

//FindAll Get all customers in db
func (d CustomerRepositoryDB) FindAll(status string) ([]Customer, *errs.AppError) {
	findAllSQL := ""
	if status == "" {
		findAllSQL = `select customer_id, name, date_of_birth, city, zipcode, status from customers where 1 = ?`
		status = "1"
	} else {
		findAllSQL = `select customer_id, name, date_of_birth, city, zipcode, status from customers where status = ?`
	}

	var customers []Customer
	err := d.client.Select(&customers, findAllSQL, status)
	if err != nil {
		logger.Error("Error [Customers]" + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}

	return customers, nil
}

//FindByID Get customer in db
func (d CustomerRepositoryDB) FindByID(id string) (*Customer, *errs.AppError) {
	var c Customer
	findAllSQL := `select customer_id, name, date_of_birth, city, zipcode, status from customers where customer_id=?`

	err := d.client.Get(&c, findAllSQL, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("Error [Customer not found]")
		}
		logger.Error("Error [Customers] while scanning" + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error - [Customer]")
	}

	return &c, nil
}
