package domain

import "gitlab.com/kokegudiel2/banking/dto"

//WITHDRAWAL type transaction
const WITHDRAWAL = "withdrawal"

//Transaction struct for control
type Transaction struct {
	TransactionID   string  `db:"transaction_id"`
	AccountID       string  `db:"account_id"`
	Amount          float64 `db:"amount"`
	TransactionType string  `db:"transaction_type"`
	TransactionDate string  `db:"transaction_date"`
}

//IsWithdrawal type
func (t Transaction) IsWithdrawal() bool {
	return t.TransactionType == WITHDRAWAL
}

//ToDTO convert Transaction to TransactionResponse
func (t Transaction) ToDTO() dto.TransactionResponse {
	return dto.TransactionResponse{
		AccountID:       t.AccountID,
		Amount:          t.Amount,
		TransactionDate: t.TransactionDate,
		TransactionID:   t.TransactionID,
		TransactionType: t.TransactionType,
	}
}
