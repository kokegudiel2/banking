package domain

import (
	"fmt"
	"strconv"

	"github.com/jmoiron/sqlx"
	"gitlab.com/kokegudiel2/banking/errs"
	"gitlab.com/kokegudiel2/banking/logger"
)

//AccountRepositoryDB struct for db operations
type AccountRepositoryDB struct {
	client *sqlx.DB
}

//Save func for save in the db
func (d AccountRepositoryDB) Save(a Account) (*Account, *errs.AppError) {
	sqlInsert := "INSERT INTO accounts (customer_id, account_type, amount, status) values (?, ?, ?, ?)"

	result, err := d.client.Exec(sqlInsert, a.CustomerID, a.AccountType, a.Amount, a.Status)
	if err != nil {
		logger.Error("Error saving account " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error from database saving account " + err.Error())
	}

	ID, err := result.LastInsertId()
	if err != nil {
		logger.Error("Error saving account " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error from database saving account " + err.Error())
	}

	a.AccountID = fmt.Sprintf("%d", ID)

	return &a, nil
}

func (d AccountRepositoryDB) SaveTransaction(t Transaction) (*Transaction, *errs.AppError) {
	tx, err := d.client.Begin()
	if err != nil {
		logger.Error("Error starting a transaction for bank account: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error database ")
	}

	result, err := tx.Exec(`INSERT INTO transactions (account_id, amount, transaction_type)
	values (?, ?, ?)`, t.AccountID, t.Amount, t.TransactionType)
	if err != nil {
		tx.Rollback()
		logger.Error("Error executing query " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error database ")
	}

	if t.IsWithdrawal() {
		_, err = tx.Exec(`UPDATE accounts SET amount = amount - ? where account_id = ?`, t.Amount, t.AccountID)
	} else {
		_, err = tx.Exec(`UPDATE accounts SET amount = amount + ? where account_id = ?`, t.Amount, t.AccountID)
	}

	if err != nil {
		tx.Rollback()
		logger.Error("Error executing query " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected error database ")
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		logger.Error("Error while commiting transaction for bank account: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}

	transactionID, err := result.LastInsertId()
	if err != nil {
		logger.Error("Error while getting the last transaction id: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}

	account, appError := d.FindBy(t.AccountID)
	if appError != nil {
		return nil, appError
	}
	t.TransactionID = strconv.FormatInt(transactionID, 10)

	t.Amount = account.Amount
	return &t, nil
}

func (d AccountRepositoryDB) FindBy(accountID string) (*Account, *errs.AppError) {
	sqlGetAccount := "SELECT account_id, customer_id, opening_date, account_type, amount from accounts where account_id = ?"
	var account Account

	err := d.client.Get(&account, sqlGetAccount, accountID)
	if err != nil {
		logger.Error("Error while fetching account information: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}

	return &account, nil
}

//NewAccountRepositoryDB Instance new repository
func NewAccountRepositoryDB(dbClient *sqlx.DB) AccountRepositoryDB {
	return AccountRepositoryDB{
		dbClient,
	}
}
