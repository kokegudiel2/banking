package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//Log global var
var log *zap.Logger

func init() {
	var err error

	config := zap.NewProductionConfig()
	enconderConfig := zap.NewProductionEncoderConfig()
	enconderConfig.TimeKey = "timestamp"
	enconderConfig.StacktraceKey = ""
	enconderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.EncoderConfig = enconderConfig

	log, err = config.Build(zap.AddCallerSkip(1))
	if err != nil {
		panic(err)
	}
}

//Info method for loggin
func Info(msg string, fields ...zap.Field) {
	log.Info(msg, fields...)
}

//Fatal method for loggin
func Fatal(msg string, fields ...zap.Field) {
	log.Fatal(msg, fields...)
}

//Debug method fro loggin
func Debug(msg string, fields ...zap.Field) {
	log.Debug(msg, fields...)
}

//Error method fro loggin
func Error(msg string, fields ...zap.Field) {
	log.Error(msg, fields...)
}
