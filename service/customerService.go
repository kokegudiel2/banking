package service

import (
	"gitlab.com/kokegudiel2/banking/domain"
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/errs"
)

//CustomerService interface behaivor
//go:generate mockgen -destination=../mocks/service/mockCustomerService.go -package=service gitlab.com/kokegudiel2/banking/service CustomerService
type CustomerService interface {
	GetAllCustomers(string) ([]dto.CustomerResponse, *errs.AppError)
	GetCustomer(string) (*dto.CustomerResponse, *errs.AppError)
}

//DefaultCustomerService struct commons
type DefaultCustomerService struct {
	repo domain.CustomerRepository
}

//GetAllCustomers return the FindAll() func from the repository
func (s DefaultCustomerService) GetAllCustomers(status string) ([]dto.CustomerResponse, *errs.AppError) {
	if status == "active" {
		status = "1"
	} else if status == "inactive" {
		status = "0"
	} else {
		status = ""
	}

	customers, err := s.repo.FindAll(status)
	if err != nil {
		return nil, err
	}
	response := make([]dto.CustomerResponse, 0)
	for _, v := range customers {
		response = append(response, v.ToDTO())
	}

	return response, nil
}

//GetCustomer return the FindByID() func from the repository
func (s DefaultCustomerService) GetCustomer(id string) (*dto.CustomerResponse, *errs.AppError) {
	c, err := s.repo.FindByID(id)
	if err != nil {
		return nil, err
	}

	r := c.ToDTO()

	return &r, nil
}

//NewCustomerService instance a custumer service ("inject")
func NewCustomerService(repository domain.CustomerRepository) DefaultCustomerService {
	return DefaultCustomerService{repo: repository}
}
