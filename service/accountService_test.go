package service

import (
	"testing"

	"github.com/golang/mock/gomock"
	mydomain "gitlab.com/kokegudiel2/banking/domain"
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/errs"
	"gitlab.com/kokegudiel2/banking/mocks/domain"
)

//TestValidateResponse should
//return a validation error response
//when the request is not validated
func TestValidateResponse(t *testing.T) {
	request := dto.NewAccountRequest{
		CustumerID:  "100",
		AccountType: "saving",
		Amount:      0,
	}

	service := NewAccountService(nil)

	_, err := service.NewAccount(request)

	if err == nil {
		t.Error("field while testing new account validation [AccountService]")
	}
}

var (
	_mockAccountRepository *domain.MockAccountRepository
	_service               AccountService
)

func setup(t *testing.T) func() {
	ctrl := gomock.NewController(t)
	_mockAccountRepository = domain.NewMockAccountRepository(ctrl)
	_service = NewAccountService(_mockAccountRepository)

	return func() {
		_service = nil
		defer ctrl.Finish()
	}
}

//TestCreateAccount should
//return an error from the server side
//if the new account cannot be created
func TestCreateAccount(t *testing.T) {
	teardown := setup(t)
	defer teardown()

	request := dto.NewAccountRequest{
		CustumerID:  "100",
		AccountType: "saving",
		Amount:      6000,
	}

	a := mydomain.Account{
		CustomerID:  request.CustumerID,
		AccountType: request.AccountType,
		Amount:      request.Amount,
		Status:      "1",
	}

	_mockAccountRepository.EXPECT().Save(a).Return(nil, errs.NewUnexpectedError("Unexpected DB Error"))

	_, err := _service.NewAccount(request)

	//Assert
	if err == nil {
		t.Error("Test failed while validating for new account")
	}

}

//TestSaveAccount should
//return new account response when
//a new account is saved successfuly
func TestSaveAccount(t *testing.T) {
	teardown := setup(t)
	defer teardown()

	request := dto.NewAccountRequest{
		CustumerID:  "100",
		AccountType: "saving",
		Amount:      6000,
	}

	a := mydomain.Account{
		CustomerID:  request.CustumerID,
		AccountType: request.AccountType,
		Amount:      request.Amount,
		Status:      "1",
	}

	accountWhitID := a
	accountWhitID.AccountID = "201"
	_mockAccountRepository.EXPECT().Save(a).Return(&accountWhitID, nil)

	newAccount, err := _service.NewAccount(request)

	if err != nil {
		t.Error("Test Failed creating new account [AccountService]")
	}

	if newAccount.AccountID != accountWhitID.AccountID {
		t.Error("Failed new account ID [AccountService - Save]")
	}
}
