package service

import (
	"gitlab.com/kokegudiel2/banking/domain"
	"gitlab.com/kokegudiel2/banking/dto"
	"gitlab.com/kokegudiel2/banking/errs"
)

//AccountService interface behaivor
type AccountService interface {
	NewAccount(dto.NewAccountRequest) (*dto.NewAccountResponse, *errs.AppError)
	MakeTransaction(dto.TransactionRequest) (*dto.TransactionResponse, *errs.AppError)
}

//DefaultAccountService struct commons
type DefaultAccountService struct {
	repo domain.AccountRepository
}

//NewAccount create new account
func (s DefaultAccountService) NewAccount(r dto.NewAccountRequest) (*dto.NewAccountResponse, *errs.AppError) {
	err := r.Validate()
	if err != nil {
		return nil, err
	}

	a := domain.Account{
		CustomerID:  r.CustumerID,
		AccountType: r.AccountType,
		Amount:      r.Amount,
		Status:      "1",
	}

	newAccount, err := s.repo.Save(a)
	if err != nil {
		return nil, err
	}

	response := newAccount.ToNewAccountResponseDTO()

	return &response, nil
}

//MakeTransaction ...
func (s DefaultAccountService) MakeTransaction(r dto.TransactionRequest) (*dto.TransactionResponse, *errs.AppError) {
	err := r.Validate()
	if err != nil {
		return nil, err
	}

	if r.IsTransactionTypeWithdrawal() {
		account, err := s.repo.FindBy(r.AccountID)
		if err != nil {
			return nil, err
		}

		if !account.CanWithdraw(r.Amount) {
			return nil, errs.NewValidationError("Insufficient balance in the account")
		}
	}

	t := domain.Transaction{
		AccountID:       r.AccountID,
		Amount:          r.Amount,
		TransactionType: r.TransactionType,
	}
	transaction, err := s.repo.SaveTransaction(t)
	if err != nil {
		return nil, err
	}

	response := transaction.ToDTO()

	return &response, nil
}

//NewAccountService instace a new service (inject)
func NewAccountService(repo domain.AccountRepository) DefaultAccountService {
	return DefaultAccountService{repo: repo}
}
