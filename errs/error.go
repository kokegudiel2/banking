package errs

import "net/http"

//AppError error handling xd
type AppError struct {
	Code    int    `json:",omitempty"`
	Message string `json:"message"`
}

//AsMessage return the message
func (e AppError) AsMessage() *AppError {
	return &AppError{
		Message: e.Message,
	}
}

//NewNotFoundError ...
func NewNotFoundError(msg string) *AppError {
	return &AppError{
		Message: msg,
		Code:    http.StatusNotFound,
	}
}

//NewUnexpectedError ...
func NewUnexpectedError(msg string) *AppError {
	return &AppError{
		Message: msg,
		Code:    http.StatusInternalServerError,
	}
}

//NewValidationError ...
func NewValidationError(msg string) *AppError {
	return &AppError{
		Message: msg,
		Code:    http.StatusUnprocessableEntity,
	}
}
