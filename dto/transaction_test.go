package dto

import (
	"testing"
)

func TestTransactionTypeValidate(t *testing.T) {
	request := TransactionRequest{
		TransactionType: "invalid",
	}

	err := request.Validate()

	//Assert
	if err != nil {
		t.Error("Invalid", err)
	}
}

func TestAmountLessThanZero(t *testing.T) {
	request := TransactionRequest{
		TransactionType: DEPOSIT,
		Amount:          -1,
	}

	err := request.Validate()

	//Assert
	if err != nil {
		t.Error("Invalid", err.Message)
	}
}
