package dto

import (
	"strings"

	"gitlab.com/kokegudiel2/banking/errs"
)

//NewAccountRequest strut for control
type NewAccountRequest struct {
	CustumerID  string  `json:"customer_id"`
	AccountType string  `json:"account_type"`
	Amount      float64 `json:"amount"`
}

//Validate for create account
func (r NewAccountRequest) Validate() *errs.AppError {
	if r.Amount < 5000 {
		return errs.NewValidationError("To open the new account you need deposit atleast 5000.00")
	}

	if strings.ToLower(r.AccountType) != "saving" &&
		strings.ToLower(r.AccountType) != "cheching" {
		return errs.NewValidationError("Type of account is invalid")
	}

	return nil
}
