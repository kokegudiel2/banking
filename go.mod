module gitlab.com/kokegudiel2/banking

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.4
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.2.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/tools v0.0.0-20201218024724-ae774e9781d2 // indirect
)
